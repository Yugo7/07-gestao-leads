package br.com.itau.lead_collector.repositories;

import br.com.itau.lead_collector.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {

}
