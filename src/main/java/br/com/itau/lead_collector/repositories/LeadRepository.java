package br.com.itau.lead_collector.repositories;

import br.com.itau.lead_collector.models.Lead;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LeadRepository extends CrudRepository<Lead, Integer> {

    List<Lead> findByCpf(String cpf);

    List<Lead> findByProdutosId(int idProduto);

}
