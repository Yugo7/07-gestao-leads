package br.com.itau.lead_collector.controllers;

import br.com.itau.lead_collector.models.Produto;
import br.com.itau.lead_collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto adicionaProduto(@RequestBody @Valid Produto produto){
        return produtoService.salvarProduto(produto);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Produto> retornaTodosProdutos(){
        return produtoService.retornaTodosProdutos();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Produto retornaProdutoPorId(@PathVariable(name = "id") int id){
        try{
            return produtoService.buscaProdutoPorId(id);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Produto atualizaProduto(@PathVariable(name = "id") int id, @RequestBody @Valid Produto produto){
        try{
            return produtoService.atualizarProduto(id, produto);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletaProduto(@PathVariable(name = "id") int id){
        try{
            produtoService.deletaProduto(id);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
