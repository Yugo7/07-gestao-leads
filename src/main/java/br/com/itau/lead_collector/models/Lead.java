package br.com.itau.lead_collector.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Entity
//@Table(name = "leads") //Sobrescreve o nome da tabela
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //@Column(name = "nome_completo") //Sobrescreve o nome da tabela
    @NotNull(message = "Nome veio como null")//Nao permite null
    @NotBlank(message = "Nome veio em branco")//Nao permite branco
    @Size(min = 3, message = "Nome veio em branco")//Nao permite tamanho menor que 3
    private String nome;

    @CPF(message = "CPF CANCELADO")
    @NotNull(message = "CPF veio como null")//Nao permite null
    private String cpf;

    @Email(message = "Email invalido")
    @NotNull(message = "Email veio como null")//Nao permite null
    private String email;

    private LocalDate dataDeCadastro;

    private String telefone;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Produto> produtos;

    public Lead(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public LocalDate getDataDeCadastro() {
        return dataDeCadastro;
    }

    public void setDataDeCadastro(LocalDate dataDeCadastro) {
        this.dataDeCadastro = dataDeCadastro;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}
