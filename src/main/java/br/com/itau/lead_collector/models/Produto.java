package br.com.itau.lead_collector.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.util.Date;

@Entity
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Nome veio como null")//Nao permite null
    @NotBlank(message = "Nome veio em branco")//Nao permite branco
    @Size(min = 3, message = "Nome veio em branco")//Nao permite tamanho menor que 3
    private String nome;

    @NotNull(message = "Descricao veio como null")//Nao permite null
    @NotBlank(message = "Descricao veio em branco")//Nao permite branco
    @Size(min = 3, message = "Descricao veio em branco")//Nao permite tamanho menor que 3
    private String descricao;

    @NotNull(message = "Nome veio como null")//Nao permite null
    @DecimalMin(value = "0.01", message = "Preco invalido")//Nao permite valor menor que 0.01
    @Digits(integer = 10, fraction = 2, message = "Maximo de 2 numero apos a virgula")
    private Double preco;

    public Produto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }
}
