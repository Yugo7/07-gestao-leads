package br.com.itau.lead_collector.services;

import br.com.itau.lead_collector.DTO.IdProdutoDTO;
import br.com.itau.lead_collector.DTO.LeadDTO;
import br.com.itau.lead_collector.models.Lead;
import br.com.itau.lead_collector.models.Produto;
import br.com.itau.lead_collector.repositories.LeadRepository;
import br.com.itau.lead_collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService  {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public Lead SalvarLead(LeadDTO leadDTO){

        Lead lead = leadDTO.converterParaLead();
        //Preencher a data
        lead.setDataDeCadastro(LocalDate.now());

        // Separando os ids para consulta dos Produtos no Banco de Dados
        List<Integer> idDeProdutos = new ArrayList<>();
        for(IdProdutoDTO idProdutoDTO : leadDTO.getProdutos()){
            int id = idProdutoDTO.getId();
            idDeProdutos.add(id);
        }
        // Pesquisa os produtos baseado no id
        Iterable<Produto> produtos = produtoRepository.findAllById(idDeProdutos);

        lead.setProdutos((List<Produto>) produtos);

        return leadRepository.save(lead);
    }

    public Iterable<Lead>  retornaTodosLeads(){
        return leadRepository.findAll();
    }

    //RuntimeException = exceção exclusiva para service
    public Lead buscarLeadPorId(int id) throws RuntimeException {
        Optional<Lead> leadOptional = leadRepository.findById(id);

        if(leadOptional.isPresent()){
            return leadOptional.get();
        } else {
            throw new RuntimeException("Lead não encontrado.");
        }
    }

    //RuntimeException = exceção exclusiva para service
    public Iterable<Lead> buscarLeadPorCpf(String cpf) throws RuntimeException {
        List<Lead> leadPorCpf = leadRepository.findByCpf(cpf);

        if(!leadPorCpf.isEmpty()){
            return leadPorCpf;
        } else {
            throw new RuntimeException("Lead não encontrado.");
        }
    }

    //RuntimeException = exceção exclusiva para service
    public Iterable<Lead> buscarLeadPorProduto(int idProduto) throws RuntimeException {
        List<Lead> leadPorProduto = leadRepository.findByProdutosId(idProduto);

        if(!leadPorProduto.isEmpty()){
            return leadPorProduto;
        } else {
            throw new RuntimeException("Produto não encontrado em nenhum lead.");
        }
    }

    //RuntimeException = exceção exclusiva para service
    public Lead atualizarLead(int id, Lead lead) throws RuntimeException {
        Lead leadDB = buscarLeadPorId(id);
        lead.setId(leadDB.getId());

        return leadRepository.save(lead);
    }

    public void deletaLead(int id) throws RuntimeException {
        if(leadRepository.existsById(id)){
            leadRepository.deleteById(id);
        } else {
            throw new RuntimeException("Lead não existe");
        }
    }



}
