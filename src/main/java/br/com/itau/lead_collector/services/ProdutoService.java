package br.com.itau.lead_collector.services;

import br.com.itau.lead_collector.models.Produto;
import br.com.itau.lead_collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto){
        return produtoRepository.save(produto);
    }

    public Iterable<Produto> retornaTodosProdutos(){
        return produtoRepository.findAll();
    }

    public Produto buscaProdutoPorId(int id) throws RuntimeException{
        Optional<Produto> produtoOptional = produtoRepository.findById(id);
        if(produtoOptional.isPresent()){
            return produtoOptional.get();
        }
        else{
            throw new RuntimeException("Produto não encontrado");
        }
    }

    public Produto atualizarProduto(int id, Produto produto) throws RuntimeException{
        Produto produtoDB = buscaProdutoPorId(id);
        produto.setId(produtoDB.getId());

        return produtoRepository.save(produto);
    }


    public void deletaProduto(int id) throws RuntimeException{
        if (produtoRepository.existsById(id)){
            produtoRepository.deleteById(id);
        } else {
            throw new RuntimeException("Produto não encontrado");
        }
    }
}
