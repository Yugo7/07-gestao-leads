package br.com.itau.lead_collector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeadCollectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeadCollectorApplication.class, args);
	}

}
