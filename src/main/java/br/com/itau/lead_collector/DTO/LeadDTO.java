package br.com.itau.lead_collector.DTO;

import br.com.itau.lead_collector.models.Lead;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class LeadDTO {
    @NotNull(message = "Nome não pode ser nullo")
    @NotBlank(message = "Não pode estar em branco")
    @Size(min = 3, message = "Nome no minimo com 3 caracteres")
    private String nome;

    @CPF(message = "CPF é invalido")
    @NotNull
    private String cpf;

    @Email(message = "Email invalido")
    @NotNull
    private String email;

    private String telefone;

    @NotNull
    private List<IdProdutoDTO> produtos;

    public LeadDTO(){

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public List<IdProdutoDTO> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<IdProdutoDTO> produtos) {
        this.produtos = produtos;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Lead converterParaLead(){
        Lead lead = new Lead();
        lead.setNome(this.nome);
        lead.setEmail(this.email);
        lead.setCpf(this.cpf);
        lead.setTelefone(this.telefone);

        return lead;
    }
}
